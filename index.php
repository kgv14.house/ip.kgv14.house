<?php

/**
* Validates given string as an IP address, returns string on success or false on failure.
*
* @param string $String
* @return mixed
*/
function Val_ip ($String) {
    // Check that given string is a valid IP number.
    if (long2ip (ip2long ($String)) != $String) {
        // Wasn't, return error.
        return false;
    }
    // Everything was OK, return string.
    return $String;
}

// Create response
$response = array(
    "remote_addr" => Val_ip($_SERVER["REMOTE_ADDR"])
);

echo json_encode($response);
?>
